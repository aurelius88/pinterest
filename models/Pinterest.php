<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 28.10.2017
 * Time: 20:03
 */

namespace app\models;

use yii\base\Model;

class Pinterest extends Model
{
    private static $instance = null;
    private $app_id = "4930896065875426484";
    private $secret_key = "097fb9cf2dcad012cf45ffd828ce824fc33fb33cbeffd77820c9d659ac8fd8dc";
    private $base_url = "https://api.pinterest.com/";
    private $token = null;


    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Pinterest();
        }
        return self::$instance;
    }

    public function getCode()
    {
        $state = uniqid();
        \Yii::$app->session->set('state', $state);
        $data = array(
            'response_type' => 'code',
            'redirect_uri' => 'https://test-laconicastudio.loc/web/index.php?r=site%2Flogin',
            'client_id' => $this->app_id,
            'scope' => 'read_public',
            'state' => $state
        );
        $query = $this->base_url.'oauth/?'.http_build_query($data);
        return \Yii::$app->getResponse()->redirect($query);
    }

    public function createToken($code)
    {
        $data = array(
            'grant_type' => 'authorization_code',
            'client_id' => $this->app_id,
            'client_secret' => $this->secret_key,
            'code' => $code
        );
        $rez = $this->request('v1/oauth/token', $data,'POST');

        if (!empty($rez)&& is_array($rez)) {
            if (!empty($rez['access_token'])) {
                $this->token = $rez['access_token'];
                \Yii::$app->session->set('access_token',$this->token);
            }
        }
    }

    /**
     * return user info
     */
    public function getUser()
    {
        $data = array(
            'access_token' => $this->token,
        );
        return $this->request('v1/me/', $data, 'GET');
    }

    /**
     * return all boards
     */
    public function getBoards()
    {
        $data = array(
            'access_token' => $this->token,
        );
        return $this->request('v1/me/boards/', $data, 'GET');
    }

    /**
     * @param int $limit
     * @return bool|mixed
     *
     * return all pins
     */
    public function getAllPins($limit = 100)
    {
        $data = array(
            'access_token' => $this->token,
            'limit' => $limit,
            'fields' => 'id,link,board,image'
        );
        return $this->request('v1/me/pins/', $data,'GET');
    }

    /**
     * @param $url
     * @param $data
     * @param string $type
     * @return bool|mixed
     *
     * generate request
     */
    private function request($url, $data, $type = 'GET')
    {
        $query = null;
        $options = null;
        $ch = curl_init();
        if ($type == 'GET') {
            $query = $this->base_url.$url.'?'.http_build_query($data);
            $options = array(
                CURLOPT_URL => $query,
                CURLOPT_RETURNTRANSFER => true
            );
        } elseif ($type == 'POST') {
            $query = $this->base_url.$url;
            $options = array(
                CURLOPT_URL => $query,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => http_build_query($data)
            );
        } else {
            die('undefined type');
        }
        curl_setopt_array($ch,$options);
        $output = curl_exec($ch);
        curl_close($ch);
        if (!empty($output)) {
            return json_decode($output, 1);
        }
        return false;
    }
}